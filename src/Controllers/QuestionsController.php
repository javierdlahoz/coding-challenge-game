<?php


namespace Ucc\Controllers;


use Ucc\Http\InteractsWithUserSession;
use Ucc\Models\Question;
use Ucc\Services\QuestionService;
use Ucc\Session;

class QuestionsController extends Controller
{

    use InteractsWithUserSession;

    /**
     * @var QuestionService
     */
    private QuestionService $questionService;

    /**
     * QuestionsController constructor.
     * @param QuestionService $questionService
     */
    public function __construct(QuestionService $questionService)
    {
        parent::__construct();
        $this->questionService = $questionService;
    }

    /**
     * @param Question $question
     * @param string $answer
     * @return bool
     */
    private function processAnswer(Question $question, string $answer): bool
    {
        if ($question->isCorrectAnswer($answer)) {
            $this->increaseUserPoints($question->getPoints());
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function beginGame(): bool
    {
        $name = $this->requestBody->name ?? null;

        if (empty($name)) {
            return $this->json(['error' => 'You must provide a name'], 400);
        }

        Session::set(self::$_NAME, $name);
        Session::set(self::$_QUESTION_COUNT, 1);

        $question = $this->questionService->getRandomQuestions(1)[0];

        return $this->json(['question' => $question], 201);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function answerQuestion(int $id): bool
    {
        if ($this->getUserName() === null) {
            return $this->json(['error' => 'You must first begin a game'], 400);
        }

        $question = $this->questionService->find($id);
        if (!$question) {
            return $this->json(['error' => 'Question not found'], 404);
        }

        $answer = $this->requestBody->answer ?? null;
        if (empty($answer)) {
            return $this->json(['error' => 'You must provide an answer'], 400);
        }

        if (in_array($id, $this->getAnsweredQuestionIds())) {
            return $this->json(['error' => 'You already answered this question'], 400);
        }

        $this->addToAnsweredQuestions($id);
        $isCorrect = $this->processAnswer($question, $answer);
        $points = $this->getUserPoints();

        if ($this->getUserQuestionsCount() > 4) {
            $name = $this->getUserName();
            Session::destroy();

            return $this->json([
                'message' => "Thank you for playing {$name}. Your total score was: {$points} points!"
            ]);
        }

        $message = $isCorrect ? 'Your answer is correct' : 'Wrong answer';
        $nextQuestion = $this->questionService->getRandomQuestions(1, $this->getAnsweredQuestionIds())[0];
        $this->increaseQuestionsCount();

        return $this->json([
            'message' => $message,
            'points' => $points,
            'question' => $nextQuestion
        ]);
    }
}