<?php


namespace Ucc\Http;


use Ucc\Session;

trait InteractsWithUserSession
{
    /**
     * @var string
     */
    public static $_ANSWERED_QUESTIONS = 'answeredQuestions';

    /**
     * @var string
     */
    public static $_QUESTION_COUNT = 'questionCount';

    /**
     * @var string
     */
    public static $_NAME = 'name';

    /**
     * @var string
     */
    public static $_POINTS = 'points';

    /**
     * @return int
     */
    private function getUserPoints(): int
    {
        return (int)Session::get(self::$_POINTS);
    }

    /**
     * @param int $points
     */
    private function increaseUserPoints(int $points): void
    {
        Session::set(self::$_POINTS, $this->getUserPoints() + $points);
    }

    /**
     * @return string|null
     */
    private function getUserName(): ?string
    {
        return Session::get(self::$_NAME);
    }

    /**
     * @return int
     */
    private function getUserQuestionsCount(): int
    {
        return (int)Session::get(self::$_QUESTION_COUNT);
    }

    /**
     * Increase the question count
     */
    private function increaseQuestionsCount(): void
    {
        Session::set(self::$_QUESTION_COUNT, $this->getUserQuestionsCount() + 1);
    }

    /**
     * @return array
     */
    private function getAnsweredQuestionIds(): array
    {
        return (array) json_decode(Session::get(self::$_ANSWERED_QUESTIONS), true);
    }

    /**
     * @param int $questionId
     */
    private function addToAnsweredQuestions(int $questionId): void
    {
        if (!in_array($questionId, $this->getAnsweredQuestionIds(), true)) {
            $answeredQuestions = $this->getAnsweredQuestionIds();
            $answeredQuestions[] = $questionId;
            Session::set(self::$_ANSWERED_QUESTIONS, json_encode($answeredQuestions));
        }
    }

}