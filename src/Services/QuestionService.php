<?php


namespace Ucc\Services;


use JsonMapper;
use KHerGe\JSON\JSON;
use Ucc\Models\Question;

class QuestionService
{
    const QUESTIONS_PATH = __DIR__ . '/../../questions.json';

    /**
     * @var JSON
     */
    private JSON $json;

    /**
     * @var JsonMapper
     */
    private JsonMapper $jsonMapper;

    /**
     * @var array
     */
    private array $collection;

    /**
     * QuestionService constructor.
     * @param JSON $json
     * @param JsonMapper $jsonMapper
     */
    public function __construct(JSON $json, JsonMapper $jsonMapper)
    {
        $this->json = $json;
        $this->jsonMapper = $jsonMapper;
        $this->setCollection(json_decode(file_get_contents(self::QUESTIONS_PATH), true));
    }

    /**
     * @param array $collection
     */
    private function setCollection(array $collection): void
    {
        $this->collection = [];
        foreach ($collection as $item) {
            $this->collection[] = new Question($item);
        }
    }

    /**
     * @param int $count
     * @param array $notInIds
     * @return array
     */
    public function getRandomQuestions(int $count = 5, array $notInIds = []): array
    {
        $randomCollection = $this->collection;
        $randomCollection = array_filter(
            $randomCollection,
            fn(Question $question) => !in_array($question->getId(), $notInIds, true)
        );

        shuffle($randomCollection);
        return array_slice($randomCollection, $count);
    }

    /**
     * @param int $id
     * @return Question|null
     */
    public function find(int $id): ?Question
    {
        $questions = array_filter($this->collection, fn(Question $question) => $question->getId() === $id);
        return empty($questions) ? null : array_slice($questions, 0)[0];
    }

    /**
     * @return Question
     */
    public function first(): Question
    {
        return $this->collection[0];
    }
}